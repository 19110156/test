import React from 'react';

import './App.css';


import HeaderHAVU from "./components/header/header";
import Footer from "./components/footer/footer";

import Porfoli from "./pages/home/section/section";
import Banner from "./components/bannerWowi/banner";
import ReasonsItem from "./components/reasons/component/reasonsItems/reasonsItem";

import image from './asset/images/banner.jpg'
import AboutusPage from "./pages/aboutus/aboutusPage";
function App() {

  return (

      <div className="App">
          <HeaderHAVU/>

         <AboutusPage/>


        <Porfoli vt1={'1'} vt2={'2'} />
          <ReasonsItem></ReasonsItem>
          <Footer/>
      </div>
  );
}

export default App;
