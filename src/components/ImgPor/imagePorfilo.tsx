import React from "react";
import style from './style/ImagePorfilo.module.scss'
import ImgPorfoli from '../../asset/images/porfilo.png'
interface img{

    image:any
}
const ImagePorfilo =({image}:img)=>{
    return(
        <div className={style.images}>
            <img src={image} alt={"Img"}/>
        </div>
    );
}
export  default  ImagePorfilo;