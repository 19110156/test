import React from "react";
import  style from './section.module.scss'
import ContentPorfilo from "../../components/contentPor/contentPorfilo";
import ImagePorfilo from "../../components/ImgPor/imagePorfilo";

interface Vt{
    vt1:String
    vt2:String
}
interface  img{
    image:any
}

const Porfoli=(vt:Vt,{image}:img)=> {
    return (
        <section className={style.porfoli}>
            <div className={style.porfoli__left && `order-lg-${vt.vt1}`}>
                <ContentPorfilo/>
            </div>

            <div className={style.porfoli__right && `order-lg-${vt.vt2}`}>
                <ImagePorfilo image={image}/>
            </div>
        </section>
    );
}
export  default  Porfoli;