import React from "react";
import style from './style/showcaseItem.module.scss'
import anh1 from '../../asset/images/showcase1.png'
const ItemsShowCase=()=>{
    return(
        <div className={style.showcaseItem}>
            <div className={style.images}>
                <img src={anh1}/>
            </div>
            <div className={style.button}>
                <p>Minimalism</p>
            </div>
        </div>
    );
}
export  default  ItemsShowCase