import React from "react";
import style from './style/banner.module.scss'
import banner from '../../asset/images/banner.jpg'


interface img{

    image:any
}
const Banner=({image}:img)=>{
    return(
        <section className={style.banner}>
            <div className={style.left}>
                <p className={style.title}>IT SERVICES BUILT</p>
                <h2>
                    WORKS WITH EXPERTS WHO CAN DELIVER WORD-CLASS SERVICE
                </h2>
                <p className={style.note}>Building A Relationship Betwwin IT Companies & Exprest</p>
            </div>
            <div className={style.right}>
                <img src={banner} alt={"imgeBanner"}/>
            </div>
        </section>
    );
}
export default Banner;