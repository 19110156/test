import React from "react";
import style from './style/footer.module.scss'
const Footer=()=>{
    return(

            <div className={style.footer}>
                <div className={style.content}>
                    <div className={style.items}>
                        <ul>
                            <li className={style.title}>QUICK LINK</li>
                            <li>Home</li>
                            <li>About Us</li>
                            <li>IT Services</li>
                            <li>Contact</li>
                        </ul>
                    </div>
                    <div className={style.items}>
                        <ul>
                            <li className={style.title}>WHAT WE DO</li>
                            <li>Web Design</li>
                            <li>Web Development</li>
                            <li>App Development</li>
                        </ul>
                    </div>
                    <div className={style.items}>
                        <ul>
                            <li className={style.title}>CONTACT</li>
                            <li>802-Akshat Tower Ahmedabad - 380054,<br></br>
                                India</li>
                            <li>About Us</li>
                            <li><i className="fa-solid fa-phone"></i></li>
                            <li><i className="fa-solid fa-envelope"></i></li>
                        </ul>
                    </div>
                    <div className={style.items}>
                        <ul>
                            <li className={style.icons} ><i className="fa-brands fa-facebook"></i></li>
                            <li className={style.icons} ><i className="fa-brands fa-twitter"></i></li>
                            <li className={style.icons} ><i className="fa-brands fa-google"></i></li>
                            <li className={style.icons} ><i className="fa-brands fa-google"></i></li>

                        </ul>
                    </div>
                </div>

            </div>

    );
}
export default Footer;