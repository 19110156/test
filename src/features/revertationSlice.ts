import {createSlice} from "@reduxjs/toolkit";
import {PayloadAction} from "@reduxjs/toolkit";
//define type of state
interface  defiState{
    count:Number[]
}
// tao ra init state
const initialState:defiState={
    count:[]
}
//tao ra slice ve 1 doi tuong
export const revertationSlice=createSlice({
    name:"vd1",
    initialState,
    reducers:{
        cong:(state,action:PayloadAction)=>{state.count.push(1);},
        tru:(state,action:PayloadAction)=>{state.count.push(2);},

    }
})

//
export  const {cong,tru}=revertationSlice.actions;



export default revertationSlice.reducer